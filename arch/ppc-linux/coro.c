/*
    Coroutine implementation for x86/linux, gcc-2.7.2

    Copyright 1999 by E. Toernig <froese@gmx.de>
    PPC version 2001 by G.Paubert <paubert@iram.es>
    Note the PPC version is not 64 bit clean, although
    it should be trivial to implement.
*/

#include <sys/types.h>
#include <unistd.h>
#include <sys/mman.h>
#include <string.h>
#include "coro.h"

#if !defined(powerpc)
#error For PowerPC only
#endif

#if !defined(__GNUC__)
#warning May break without gcc.  Be careful.
#endif

#ifndef PAGESIZE
#define PAGESIZE	4096
#endif

struct coroutine co_main[1] = { { 0 } };
struct coroutine *co_current = co_main;


#define fatal(msg) \
    for (;;)						\
    {							\
	write(2, "coro: " msg "\r\n", sizeof(msg)+7);	\
	*(unsigned int *)0 = 0xfee1dead;		\
    }



/*
    Create new coroutine.
    'func' is the entry point
    'stack' is the start of the coroutines stack.  if 0, one is allocated.
    'size' is the size of the stack
*/


static void wrap(void *data) __attribute__((noreturn));

static void
wrap(void *data) 
{
    co_current->resumeto = co_current->caller;

    for (;;)
	data = co_resume(co_current->func(data));
}

/* Registers to save/restore: 
 * cr + r14-r31 + fr14-fr31 + fpscr + return address + ABI stack overhead
 * Layout of the co_call frame, similar to the SVR4 ABI layout:
 * Offset from SP
 * 32 bit  64 bit    content
 *      0       0    SP back chain
 *      4       8    LR save slot for callee
 *      8      16    FPSCR ?
 *     16      24    padding
 *     20      28    CR save
 *     24      32    GPR14
 *     28      40    GPR15
 *    ...     ...
 *     92     168    GPR31 
 *     96     176    FPR14
 *    104     184    FPR15
 *    ...     ...
 *    232     312    FPR31
 *    240     320    caller's SP back chain
 *    244     328    LR save slot
 * Size: 18*sizeof(double)+20*sizeof(void *)+sizeof(int)
 */

/* FIXME: these happen to sum up correctly for alignment but it's pure luck */
struct _frame_head {
    u_long backptr;
    u_long lrsave;
};

struct _co_state {
    double fpscr;
    u_int32_t __pad;
    u_int32_t cr;
    u_long gprs[18];
    double fprs[18];
};

struct _top_frames {
    struct _frame_head current;
    struct _co_state state;
    struct _frame_head caller;
};

struct coroutine *
co_create(void *func, void *stack, int size)
{
    struct coroutine *co;
    int to_free = 0;
    struct _top_frames *fp;

    if (size < 1024)
	return 0;

    if (stack == 0)
    {
	size += PAGESIZE-1;
	size &= ~(PAGESIZE-1);
	stack = mmap(0, size, PROT_READ|PROT_WRITE,
			      MAP_PRIVATE|MAP_ANON, -1, 0);
	if (stack == (void*)-1)
	    return 0;

	to_free = size;
    }
    co = stack + size;
    (unsigned long)co &= ~(sizeof(u_long)-1);
    co -= 1;
    fp = (struct _top_frames *) co - 1;
    (unsigned long) fp &= ~15;
    co->sp = fp;

    co->caller = 0;
    co->resumeto = 0;
    co->user = 0;
    co->func = func;
    co->to_free = to_free;

    /* Only a few entries have to be initialized to non zero values */
    memset(fp, 0, sizeof(*fp));
    fp->caller.lrsave = (u_long) wrap; // store return addr (here: start addr)
    fp->current.backptr = (u_long) &fp->caller;
    /* Note that coroutine start will all floating point exceptions
     * disabled and a clean FP state (no sticky exception flags set
     * IEEE compliant handling of denormals and rounding to nearest). 
     */
    return co;
}



/*
    delete a coroutine.
*/

void
co_delete(struct coroutine *co)
{
    if (co == co_current)
	fatal("coroutine deletes itself");

    if (co->to_free)
	munmap((void *)co + sizeof(*co) - co->to_free, co->to_free);
}



/*
    delete self and switch to 'new_co' passing 'data'
*/

static void *helper_args[2];

static void
del_helper(void **args)
{
    for (;;)
    {
	if (args != helper_args)
	    fatal("resume to deleted coroutine");
	co_delete(co_current->caller);
	args = co_call(args[0], args[1]);
    }
}

void
co_exit_to(struct coroutine *new_co, void *data)
{
    static struct coroutine *helper = 0;
    // be generous, only one copy is needed and a signal handler
    // with Altivec and FPU will already use about 1kB.
    static char stk[2048]; 

    helper_args[0] = new_co;
    helper_args[1] = data;

    if (helper == 0)
	helper = co_create(del_helper, stk, sizeof(stk));

    // we must leave this coroutine.  so call the helper.
    co_call(helper, helper_args);
    fatal("stale coroutine called");
}

void
co_exit(void *data)
{
    co_exit_to(co_current->resumeto, data);
}



/*
    Call other coroutine.
    'new_co' is the coroutine to switch to
    'data' is passed to the new coroutine
*/

/* BUGS and MISFEATURES: 
 * - the offsets are hardwired for the 32 bit version,
 * - the access to co_current is only correct for 32 bit address space,
 * - stw/lwz for integer save/restore are 32 bit only too,
 * - Altivec is not supported. 
 */
//void *co_call(struct coroutine *new_co, void *data) { magic }
asm("\
	.text								\n\
	.globl	co_call							\n\
	.type	co_call,@function					\n\
co_call:								\n\
	stwu	1,-240(1)						\n\
	lis	5,co_current@ha						\n\
	mflr	0							\n\
	lwz	6,co_current@l(5)					\n\
	mfcr	7							\n\
	stw	0,244(1)						\n\
	.irp	rn,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31\n\
	stfd 	\\rn,8*\\rn-16(1)					\n\
	.endr								\n\
	mffs 31								\n\
	stw 7,20(1)							\n\
	stfd 31,8(1)							\n\
	.irp	rn,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31\n\
	stw	\\rn,4*\\rn-32(1)					\n\
	.endr								\n\
	stw	1,0(6)		# switch stacks				\n\
	lwz	1,0(3)							\n\
	stw	3,co_current@l(5)					\n\
	lwz	0,244(1)						\n\
	stw	6,4(3)		# set caller field			\n\
	mr	3,4							\n\
	lfd	31,8(1)							\n\
	.irp	rn,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31\n\
	lwz	\\rn,4*\\rn-32(1)					\n\
	.endr								\n\
	mtlr	0							\n\
	lwz	0,20(1)							\n\
	mtfsf	0xff,31							\n\
	.irp	rn,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31\n\
	lfd	\\rn,8*\\rn-16(1)					\n\
	.endr								\n\
	mtcr	0							\n\
	addi	1,1,240							\n\
	blr								\n\
	.size co_call,.-co_call						\n\
");

void *
co_resume(void *data)
{
    data = co_call(co_current->resumeto, data);
    co_current->resumeto = co_current->caller;
    return data;
}
